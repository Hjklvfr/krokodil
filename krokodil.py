import os
from shutil import copyfile

import openpyxl
from openpyxl.styles import Border, Side, Font, Alignment
from openpyxl.utils import get_column_letter


def process(file_name: str):
    workbook = openpyxl.load_workbook(f'./{file_name}')
    worksheet = workbook.worksheets[0]

    worksheet.delete_cols(1, 6)
    worksheet.delete_cols(worksheet.max_column - 1)

    worksheet.insert_cols(0)
    worksheet['A1'] = 'id'

    worksheet.insert_cols(worksheet.max_column)
    worksheet.delete_cols(worksheet.max_column - 1)
    worksheet.cell(1, worksheet.max_column, "Общая средняя оценка студента")

    worksheet.insert_rows(worksheet.max_row)
    for i in range(1, worksheet.max_column + 1):
        worksheet.cell(row=worksheet.max_row - 1, column=i, value=worksheet.cell(row=worksheet.max_row, column=i).value)
    worksheet['A' + str(worksheet.max_row)] = "Среднее"

    for col in worksheet.iter_cols(min_col=2, min_row=2, max_col=worksheet.max_column - 1):
        col_sum = 0.0
        col_count = 0
        for cell in col:
            if cell.row == worksheet.max_row:  # last row
                try:
                    cell.value = round(col_sum / col_count, 1)
                    cell.number_format = '0.0'
                except ZeroDivisionError:
                    cell.value = '-'
                finally:
                    break
            if cell.value != '-':
                col_sum += cell.value
                col_count += 1

    row_id = 1
    worksheet.row_dimensions[1].height = float(80)
    for row in worksheet.iter_rows():
        if row[0].row == 1:  # header
            for cell in row:
                cell.value = str(cell.value) \
                    .replace('Тест:', '') \
                    .replace('(Значение)', '')
                cell.alignment = Alignment(wrap_text=True)
                if (letter := cell.column_letter) == 'A':
                    worksheet.column_dimensions[letter].width = float(7)
                elif letter == get_column_letter(worksheet.max_column):
                    worksheet.column_dimensions[letter].width = float(12)
                else:
                    worksheet.column_dimensions[letter].width = float(14)
            continue

        row_sum = 0.0
        row_count = 0
        for cell in row:
            if cell.column == 1:  # id
                cell.value = row_id
                row_id += 1
                continue
            if cell.column == worksheet.max_column:  # last col
                try:
                    cell.value = round(row_sum / row_count, 5)
                    cell.number_format = '0.0'
                except ZeroDivisionError:
                    cell.value = '-'
                finally:
                    break
            if cell.value != '-' and cell.value is not None:
                row_sum += cell.value
                row_count += 1

    worksheet['A' + str(worksheet.max_row)] = "Среднее"

    font = Font(name='Arial',
                size=10)
    thin_border = Border(left=Side(style='thin'),
                         right=Side(style='thin'),
                         top=Side(style='thin'),
                         bottom=Side(style='thin'))
    for row in worksheet.iter_rows():
        for cell in row:
            cell.border = thin_border
            cell.font = font

    workbook.save(f'./{file_name}')
    if not os.path.exists(folder := './processed'):
        os.makedirs(folder)
    copyfile(f'./{file_name}', f'./processed/{file_name}')


files_to_process = []
for filename in os.listdir('.'):
    if os.path.isfile(filename):
        if filename == os.path.basename(__file__) or not filename.endswith('.xlsx'):
            continue
        files_to_process.append(filename)
for file in files_to_process:
    process(file)

